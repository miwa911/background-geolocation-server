FROM node:8.11.1-alpine

# Create app directory
WORKDIR /app

COPY ./package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY ./ .

CMD [ "npm", "start" ]
